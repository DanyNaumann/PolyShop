describe('PolyShop announces', () => {
    beforeEach(() => {
        cy.visit('https://localhost/PolyShop/account/login')
        cy.get('#i1').type('micheldupont@gmail.com', {force: true})
        cy.get('#password').type('12345678{enter}', {force: true})
    })
  
    it('Creation of an announce', () => {
        cy.get(':nth-child(2) > .navbarNoPadding').click({force: true})
        cy.get('#nom').type('iPhone 11', {force: true})
        cy.get('#description').type('Vend un iPhone 11, bon état, quelques rayures sur le dos', {force: true})
        cy.get(':nth-child(3) > .form-control').type('100', {force: true})
        cy.get('#btnLogin').click({force: true})
        cy.get('#closeBanner').contains('Votre annonce a bien été créée !')
    })

    it('Edit an announce', () => {
        cy.get('.dropdown-toggle').click({force: true})
        cy.get('.dropdown-menu > :nth-child(3) > a').click({force: true})
        cy.get('[onclick="location.href=\'https://localhost/PolyShop/annonce/17\'"] > .editActivityLink > a').click({force: true})
        cy.get('#nom').type('1{enter}', {force: true})
        cy.get('#closeBanner').contains('Votre annonce a bien été modifiée !')
    })

})