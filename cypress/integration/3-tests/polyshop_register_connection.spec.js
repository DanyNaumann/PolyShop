describe('PolyShop Register and Connection', () => {
      
    it('Getting started with Cypress', () => {
        cy.visit('https://localhost/PolyShop')
        cy.get('h1').contains('PolyShop')
    })

    it('Create an account on PolyShop', () => {
        cy.visit('https://localhost/PolyShop/account/register')
        cy.get('#p1').type('John', {force: true})
        cy.get('#n1').type('Smith', {force: true})
        cy.get('#email').type('johnsmith@gmail.com', {force: true})
        cy.get('#password1').type('12345678', {force: true})
        cy.get('#password2').type('12345678{enter}', {force: true})
        cy.get('.login-box-msg').contains('Se connecter')
    })

    it('Connection on PolyShop', () => {
        cy.visit('https://localhost/PolyShop/account/login')
        cy.get('#i1').type('johnsmith@gmail.com', {force: true})
        cy.get('#password').type('12345678{enter}', {force: true})
        cy.get('.dropdown-toggle > :nth-child(1)').contains('John')
    })

})