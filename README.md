# PolyShop

This is the source code of the PolyShop application

## Description

PolyShop is a platform to help you to sell your stuff. There is multiple functionalities :

    - Create your account
    - Edit your account
    - Create your announces
    - Edit your announces
    - Delete your announces
    - Make a bid for a product

## Requirements

Some libraries are installed to work with this application. There are used to develop and test.

Install all the libraries by typing :

```bash
composer i
```

Install xampp to host locally your app :

'https://www.apachefriends.org/index.html'

Install Visual Studio Code to develop : 

'https://code.visualstudio.com/download'


## Tests

You can launch tests with two libraries, the first one is PHPUnit.


```bash
phpunit pdoAnnonceTest
```

The second one is cypress :

```bash
npm install cypress
```

```bash
./node_modules/.bin/cypress
```

And run tests.

