<?php

// With no defined action, we just show the homepage
if (!isset($_REQUEST['action'])) {
    $_REQUEST['action'] = 'homepage';
}

$action = $_REQUEST["action"];

switch ($action) {
    case "new":
        if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'homepage');
            die();
        } else {
            include 'View/annonce/new.php';
        }
        break;

    case "new_result":
        if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'homepage');
            die();
        } else {
            $name = $_POST['nom'];
            $description = $_POST['description'];
            $prix = $_POST['prix'];

            $data = $_FILES['avatar'];


            if(!empty($name) && !empty($description) && !empty($prix)){
                pdoAnnonce::addAnnonce($name, $description, $prix, 'tempo' . $_SESSION['id'] . '.png', $_SESSION['id']);

                $photo = pdoAnnonce::getAnnonceByPhotoName('tempo' . $_SESSION['id'] . '.png');

                $chemin = 'webroot/images/avatars/' . $photo[0]['id'] . '.png';

                unlink($chemin);

                $resultat = move_uploaded_file($data['tmp_name'], $chemin);

                pdoAnnonce::updatePhotoName($photo[0]['id'] . '.png', $photo[0]['id']);

                $_SESSION['flash'] = array("Votre annonce a bien été créée !", 'success');

                header('Location:' . BASE_URL . 'homepage');
            }
            else{
                header('Location:' . BASE_URL . 'homepage');
            }

        }
        break;

    case "me":
        if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'homepage');
            die();
        } else {

            $act = pdoAnnonce::getAnnonceByUser($_SESSION['id']);

            include 'View/annonce/me.php';
        }
        break;

    case "view_annonce":
        if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'homepage');
            die();
        } else {

            $actId = $_GET['id'];

            $act = pdoAnnonce::getAnnonceById($actId);

            $user = pdoUser::getUserById($act[0]['id_proprio']);

            include 'View/annonce/annonce.php';
        }
        break;

    case "edit":
        if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'homepage');
            die();
        } else {

            $actId = $_GET['id'];

            $act = pdoAnnonce::getAnnonceById($actId);

            include 'View/annonce/edit.php';
        }
        break;

    case "edit_result":
        if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'homepage');
            die();
        } else {
            $id = $_GET['id'];
			
			if(isset($_POST['nom']) && isset($_POST['description']) && isset($_POST['prix'])){
				if(!empty($_POST['nom']) && !empty($_POST['description']) && !empty($_POST['prix'])){
					$nom = $_POST['nom'];
					$description = $_POST['description'];
					$prix = $_POST['prix'];

                    $data = $_FILES['avatar'];
                    
                    if(!empty($data['name'])){

                        $chemin = 'webroot/images/avatars/' . $id . '.jpg';

                        unlink($chemin);

                        $resultat = move_uploaded_file($data['tmp_name'], $chemin);

                    }

					pdoAnnonce::updateAnnonceById($id, $nom, $description, $prix, $id . '.jpg');

					$_SESSION['flash'] = array("Votre annonce a bien été modifiée !", 'success');

					header('Location:' . BASE_URL . 'annonce/' . $id);
				}
				else{
					header('Location:' . BASE_URL . 'annonce/' . $id);
				}
			}
			else{
				header('Location:' . BASE_URL . 'annonce/' . $id);
			}
            
        }
        break;

    case "delete":
        if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'homepage');
            die();
        } else {
            $id = $_GET['id'];
            pdoAnnonce::deleteAnnonceById($id);

            header('Location:' . BASE_URL . 'annonce/me');
        }
        break;

    case "all":
        if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'homepage');
            die();
        } else {
            $act = pdoAnnonce:: getAllAnnonceExceptConnected($_SESSION['id']);
            include 'View/annonce/all.php';
        }
        break;

    case "offer":
        if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'homepage');
            die();
        } else {
            $id = $_GET['id'];
            $offer = $_POST['offer'];
            $user = $_SESSION['id'];
            
            pdoAnnonce::makeOffer($id, $user, $offer);

            header('Location:' . BASE_URL . 'annonce/' . $id);
        }
        break;

    case "see_offer":
        if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'homepage');
            die();
        } else {
            $id = $_GET['id'];
            $offer = pdoAnnonce::getOfferByIdAnnonce($id);
            for($i = 0; $i < count($offer); $i++){
                $user[$i] = pdoUser::getUserById($offer[$i]['id_user']);
            }
            
            include 'View/annonce/offer.php';
        }
        break;

    case "delete_offer":
        if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'homepage');
            die();
        } else {
            $id_act = $_GET['id_act'];
            $id_user = $_GET['id_user'];
            $montant = $_GET['montant'];

            pdoAnnonce::deleteOffer($id_act, $id_user, $montant);

            header('Location:' . BASE_URL . 'annonce/offer/' . $id_act);
        }
        break;

    case "accept_offer":
        if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'homepage');
            die();
        } else {
            $id_act = $_GET['id_act'];
            $id_user = $_GET['id_user'];
            $montant = $_GET['montant'];

            pdoAnnonce::deleteOffer($id_act, $id_user, $montant);
            pdoAnnonce::deleteAnnonceById($id_act);

            header('Location:' . BASE_URL . 'annonce/me');
        }
        break;

    //If there is no action, we show the homepage
    default:
        header('Location:' . BASE_URL . 'homepage');
        die();
        break;
}

?>