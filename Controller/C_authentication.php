<?php

// With no defined action, we just show the homepage
if (!isset($_REQUEST['action'])) {
    $_REQUEST['action'] = 'homepage';
}

$action = $_REQUEST["action"];

switch ($action) {
    case "login":
        if (pdoAuthentication::isConnected() || pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'homepage');
            die();
        } else {
            include 'View/login.php';
        }
        break;

	case "login_send":
        
        if (pdoAuthentication::isConnected() || pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'homepage');
            die();
        } else {

            if(isset($_POST['email']) && isset($_POST['email'])){
                $mail = $_POST['email'];
                $mdp = $_POST['password'];
            }
            else{
                $mail = "";
                $mdp = "";
            }
            

            $res = pdoAuthentication::login($mail);

            if (!$res)
            {
                $messageErreur = "Your email address or your password is not correct. Retry please";
                include 'View/login.php';
            }
            else
            {
                // Comparaison du pass envoyé via le formulaire avec la base
                $isPasswordCorrect = password_verify($mdp, $res[0]['password']);

                if ($isPasswordCorrect) {
                    $_SESSION['id'] = $res[0]['id'];
                    $_SESSION['name'] = $res[0]['nom'];
                    $_SESSION['firstname'] = $res[0]['prenom'];

                    header('Location:' . BASE_URL . 'homepage');

                }
                else {
                    $messageErreur = "Your email address or your password is not correct. Retry please";
                    include 'View/login.php';
                }
                
            }

        }

        break;

    // If the user want to logout
    case "logout":

        if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'account/login');
            die();
        } else {
            $_SESSION = array();

            pdoAuthentication::logout();

            setcookie('id', '', time() - 3600, '/');
            setcookie('firstname', '', time() - 3600, '/');

            header('Location:' . BASE_URL . 'homepage');

        }

        break;

    //If there is no action, we show the homepage
    default:
        header('Location:' . BASE_URL . 'homepage');
        die();
        break;
}

?>