<?php

// With no defined action, we just show the homepage
if (!isset($_REQUEST['action'])) {
    $_REQUEST['action'] = 'homepage';
}

$action = $_REQUEST["action"];

switch ($action) {
    case "register":
        if (pdoAuthentication::isConnected() || pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'homepage');
            die();
        } else {
            include 'View/user/register.php';
        }
        break;

    case "register_send":
        if (pdoAuthentication::isConnected() || pdoAuthentication::isConnectedCookie()) {
            header('Location:' . BASE_URL . 'homepage');
            die();
        } else {
            $mdp = $_POST['password'];
			$mdp2 = $_POST['password2'];
			$name = $_POST['textName'];
			$firstname = $_POST['textFirstName'];
			$mail = $_POST['email'];

			if($mdp != $mdp2){
				$messageErreur = "Both passwords are not equals";
				include 'View/user/register.php';
			}
			else{
				if(strlen($mdp) < 8){
					$messageErreur = "8 characters are required for password";
					include 'View/user/register.php';
				}
				else{
					if(pdoUser::checkMailExists($mail)[0][0] != 0){
						
                        $messageErreur = "This mail already exists";
                        include 'View/user/register.php';
					}
					else{
						$pass_hache = password_hash($mdp, PASSWORD_DEFAULT);

						if(!empty($pass_hache) && !empty($name) && !empty($firstname) && !empty($mail)){
							pdoUser::insertUser($firstname, $name, $mail, $pass_hache, 'default.jpg');

							$_SESSION['flash'] = array("Votre compte a bien été créé !", 'success');

							header('Location:' . BASE_URL . 'account/login');
						}
						else{
							header('Location:' . BASE_URL . 'homepage');
						}

					}
				}
			}
        }
        break;

	case "me":
		if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
			header('Location:' . BASE_URL . 'homepage');
			die();
		} else {
			$profil = pdoUser::getUserById($_SESSION['id']);
			include 'View/user/profile.php';
		}
		break;

	case "edit":
		if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
			header('Location:' . BASE_URL . 'homepage');
			die();
		} else {
			$id = $_SESSION['id'];
			$profil = pdoUser::getUserById($_SESSION['id']);
			include 'View/user/edit.php';
		}
		break;

	case "edit_result":
		if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
			header('Location:' . BASE_URL . 'homepage');
			die();
		} else {
			$id = $_SESSION['id'];
			
			if(isset($_POST['textName']) && isset($_POST['textFirstName']) && isset($_POST['email'])){
				if(!empty($_POST['textName']) && !empty($_POST['textFirstName']) && !empty($_POST['email'])){
					$nom = $_POST['textName'];
					$prenom = $_POST['textFirstName'];
					$mail = $_POST['email'];

					$data = $_FILES['avatar'];

					$chemin = 'webroot/images/' . $_SESSION['id'] . '.jpg';

					unlink($chemin);

					$resultat = move_uploaded_file($data['tmp_name'], $chemin);
					
					pdoUser::updateUserById($id, $nom, $prenom, $mail, $_SESSION['id'] . '.jpg');

					$_SESSION['flash'] = array("Votre profil a bien été modifié !", 'success');

					header('Location:' . BASE_URL . 'account/me');
				}
				else{
					header('Location:' . BASE_URL . 'account/me');
				}
			}
			else{
				header('Location:' . BASE_URL . 'account/me');
			}
			
		}
		break;

	case "delete":
		if (!pdoAuthentication::isConnected() && !pdoAuthentication::isConnectedCookie()) {
			header('Location:' . BASE_URL . 'homepage');
			die();
		} else {
			$id = $_SESSION['id'];
			pdoUser::deleteUser($id);
			pdoAnnonce::deleteAnnonceByIdProprio($id);
			session_destroy();

			header('Location:' . BASE_URL . 'homepage');
		}
		break;

    //If there is no action, we show the homepage
    default:
        header('Location:' . BASE_URL . 'homepage');
        die();
        break;
}

?>