<?php

use PHPUnit\Framework\TestCase;

require 'Model/pdoAnnonce.php';

class pdoAnnonceTest extends TestCase{

    // Nous avons au préalable inséré 3 annonces dans la base de données
    // Nous testons qu'il y a bien 3 annonces dans la base de données
    public function testGetAllAnnonce(){
        $res = pdoAnnonce::getAllAnnonce();

        $this->assertEquals(3, count($res));
        $this->assertNotEquals(4, count($res));
    }

    // Nous partons du principe qu'il y a au préalable 3 annonces dans la base de données
    // Nous testons qu'il y a 3 annonces dans la base de données puis testons après en avoir ajouté une qu'il y en a 4 et que les champs sont corrects
    public function testAddAnnonce(){
        $res = pdoAnnonce::getAllAnnonce();

        $this->assertEquals(3, count($res));

        pdoAnnonce::addAnnonce("iPhone", "iPhone 13 état neuf", 200, "iphone.jpg", 1);

        $resF = pdoAnnonce::getAllAnnonce();

        $this->assertEquals(4, count($resF));

        $this->assertEquals('iPhone', $resF[3]['nom']);
        $this->assertEquals('iPhone 13 état neuf', $resF[3]['description']);
        $this->assertEquals(200, $resF[3]['prix']);
        $this->assertEquals('iphone.jpg', $resF[3]['photo']);
        $this->assertEquals(1, $resF[3]['id_proprio']);

        pdoAnnonce::deleteAnnonceByName("iPhone");

    }

    // Nous ajoutons une annonce avec pour propriétaire l'utilisateur avec l'id 26 puis nous comparons que nous retrouvons cette annonce
    public function testGetAnnonceByUser(){
        pdoAnnonce::addAnnonce("iPhone", "iPhone 13 état neuf", 200, "iphone.jpg", 26);

        $res = pdoAnnonce::getAnnonceByUser(26);

        $this->assertEquals(26, $res[0]['id_proprio']);
        $this->assertEquals('iPhone', $res[0]['nom']);
        $this->assertEquals('iPhone 13 état neuf', $res[0]['description']);
        $this->assertEquals('iphone.jpg', $res[0]['photo']);
        $this->assertEquals(200, $res[0]['prix']);

        pdoAnnonce::deleteAnnonceByName("iPhone");
    }

}