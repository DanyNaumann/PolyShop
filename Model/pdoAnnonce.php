<?php
include_once 'pdoConnexion.php';

class PdoAnnonce {

    public static function getAllAnnonce(){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("SELECT * from annonce");

            $req->execute();

            $res = $req->fetchAll();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function addAnnonce($name, $description, $prix, $photo, $id_proprio){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("INSERT INTO annonce (nom, description, prix, photo, id_proprio) VALUES (:nom, :description, :prix, :photo, :id_proprio)");
            
            $req->bindParam(':nom', $name, PDO::PARAM_STR);
            $req->bindParam(':description', $description, PDO::PARAM_STR);
            $req->bindParam(':prix', $prix, PDO::PARAM_INT);
            $req->bindParam(':photo', $photo, PDO::PARAM_STR);
            $req->bindParam(':id_proprio', $id_proprio, PDO::PARAM_STR);
            
            $res = $req->execute();
            
            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function deleteAnnonceByName($name){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("DELETE FROM annonce WHERE nom = :nom");
            
            $req->bindParam(':nom', $name, PDO::PARAM_STR);
            
            $res = $req->execute();
            
            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function updatePhotoName($photo, $id){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("UPDATE annonce SET photo = :photo WHERE id = :id");
            
            $req->bindParam(':photo', $photo, PDO::PARAM_STR);
            $req->bindParam(':id', $id, PDO::PARAM_INT);
            
            $res = $req->execute();
            
            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function getAnnonceByPhotoName($photo){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("SELECT id FROM annonce WHERE photo = :photo");
            
            $req->bindParam(':photo', $photo, PDO::PARAM_STR);
            
            $req->execute();

            $res = $req->fetchAll();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function getAnnonceByUser($id){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("SELECT * FROM annonce WHERE id_proprio = :id");
            
            $req->bindParam(':id', $id, PDO::PARAM_INT);
            
            $req->execute();

            $res = $req->fetchAll();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function getAnnonceById($id){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("SELECT * FROM annonce WHERE id = :id");
            
            $req->bindParam(':id', $id, PDO::PARAM_INT);
            
            $req->execute();

            $res = $req->fetchAll();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function updateAnnonceById($id, $name, $description, $prix, $photo){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("UPDATE annonce SET nom = :name, description = :description, prix = :prix, photo = :avatar WHERE id = :id;");
            
            $req->bindParam(':id', $id, PDO::PARAM_INT);
            $req->bindParam(':name', $name, PDO::PARAM_STR);
            $req->bindParam(':description', $description, PDO::PARAM_STR);
            $req->bindParam(':prix', $prix, PDO::PARAM_STR);
            $req->bindParam(':avatar', $photo, PDO::PARAM_STR);

            $res = $req->execute();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function deleteAnnonceByIdProprio($id_proprio){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("DELETE FROM annonce WHERE id_proprio = :id;");
            
            $req->bindParam(':id', $id_proprio, PDO::PARAM_INT);

            $res = $req->execute();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function deleteAnnonceById($id){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("DELETE FROM annonce WHERE id = :id;");
            
            $req->bindParam(':id', $id, PDO::PARAM_INT);

            $res = $req->execute();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function getAllAnnonceExceptConnected($id){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("SELECT * FROM annonce WHERE id_proprio != :id");
            
            $req->bindParam(':id', $id, PDO::PARAM_INT);

            $req->execute();

            $res = $req->fetchAll();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function makeOffer($id_act, $id_user, $montant){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("INSERT INTO offer VALUES (:id_user, :id_act, :montant)");
            
            $req->bindParam(':id_act', $id_act, PDO::PARAM_INT);
            $req->bindParam(':id_user', $id_user, PDO::PARAM_INT);
            $req->bindParam(':montant', $montant, PDO::PARAM_INT);
            
            $req->execute();

            $res = $req->fetchAll();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function getOfferByIdAnnonce($id_act){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("SELECT * FROM offer WHERE id_act = :id_act ORDER BY montant DESC");
            
            $req->bindParam(':id_act', $id_act, PDO::PARAM_INT);
            
            $req->execute();

            $res = $req->fetchAll();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function deleteOffer($id_act, $id_user, $montant){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("DELETE FROM offer WHERE id_act= :id_act AND id_user = :id_user AND montant = :montant");
            
            $req->bindParam(':id_act', $id_act, PDO::PARAM_INT);
            $req->bindParam(':id_user', $id_user, PDO::PARAM_INT);
            $req->bindParam(':montant', $montant, PDO::PARAM_INT);
            
            $req->execute();

            $res = $req->fetchAll();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

}

?>