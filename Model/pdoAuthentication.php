<?php
include_once 'pdoConnexion.php';

class PdoAuthentication {

    public static function isConnected(){
        return (isset($_SESSION['id']) && isset($_SESSION['firstname']));
    }

    public static function isConnectedCookie() {
        return (isset($_COOKIE["id"]) && isset($_COOKIE["firstname"]));
    }

    public static function login($mail){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("SELECT id, password, nom, prenom FROM user WHERE mail = :mail;");

            $req->bindParam(':mail', $mail, PDO::PARAM_STR);

            $req->execute();

            $res = $req->fetchAll();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function logout(){
        session_destroy();
    }

}

?>