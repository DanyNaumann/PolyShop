<?php
include_once 'pdoConnexion.php';

class PdoUser {

    public static function getAllUser(){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("SELECT * from user");

            $req->execute();

            $res = $req->fetchAll();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function checkMailExists($mail){

        try {

            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("SELECT COUNT(nom) FROM user WHERE mail = :mail");

            $req->bindParam(':mail', $mail, PDO::PARAM_STR);

            $req->execute();

            $res = $req->fetchAll();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }  

    public static function insertUser($firstName, $name, $mail, $pass, $photo){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("INSERT INTO user(prenom, nom, mail, password, avatar) 
                                VALUES(:firstname, :name, :mail, :pass, :avatar);");

            $req->bindParam(':name', $name, PDO::PARAM_STR);
            $req->bindParam(':firstname', $firstName, PDO::PARAM_STR);
            $req->bindParam(':pass', $pass, PDO::PARAM_STR);
            $req->bindParam(':mail', $mail, PDO::PARAM_STR);
            $req->bindParam(':avatar', $avatar, PDO::PARAM_STR);

            $res = $req->execute();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function updateUserById($id, $name, $firstName, $mail, $photo){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("UPDATE user SET nom = :name, prenom = :firstname, mail = :mail, avatar = :avatar WHERE id = :id;");
            
            $req->bindParam(':id', $id, PDO::PARAM_INT);
            $req->bindParam(':name', $name, PDO::PARAM_STR);
            $req->bindParam(':firstname', $firstName, PDO::PARAM_STR);
            $req->bindParam(':mail', $mail, PDO::PARAM_STR);
            $req->bindParam(':avatar', $photo, PDO::PARAM_STR);

            $res = $req->execute();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function getUserById($id){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("SELECT * FROM user WHERE id = :id;");
            
            $req->bindParam(':id', $id, PDO::PARAM_INT);

            $req->execute();

            $res = $req->fetchAll();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

    public static function deleteUser($id_proprio){

        try {
            $objPdo = PdoConnexion::getPdoConnexion();
            $req=$objPdo->prepare("DELETE FROM user WHERE id = :id;");
            
            $req->bindParam(':id', $id_proprio, PDO::PARAM_INT);

            $res = $req->execute();

            $req->closeCursor();

            return $res;
        } 

        catch (Exception $ex) {
            return false;
        }

    }

}

?>