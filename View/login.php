<html>

<head>

<?php include 'include/default.php'; ?>

</head>

<body>

<?php include 'include/navigation.php'; ?>

<section id="section3" class="container">
    <div class="loginForm">
        <form method="post" id="formLogin" action="<?php echo BASE_URL; ?>account/login/send">
            <h2 class="login-box-msg">Se connecter</h2>
            <?php if (isset($messageErreur)) {
                echo '<div id="erreur2"><p>' . $messageErreur . '</p></div>';
            } ?>
            <div class="mailForm">
                <div class="form-group has-feedback has-float-label">
                    <input type="email" name="email" class="form-control" placeholder="Adresse mail" id="i1">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" name="password" class="form-control" placeholder="Mot de passe" id="password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
            </div>
            <div class="row">
                <button type="submit" id="btnLogin" class="btn btn-primary btn-block sign-in">
                    Se connecter
                </button>

            </div>

            <input id="sign" name="sign" type="hidden" value="in">

            <div class="row login-msg">
                <span>Pas de compte ? <a href="<?php echo BASE_URL; ?>account/register">Créer un compte</a></span>
            </div>
        </form> 
    </div>
</section>

</body>

</html>