<html>

<head>

<?php include('include/default.php'); ?>

</head>

<body>

    <?php include('include/navigation.php'); ?>

    <section id="section1">
        <img src="<?php echo BASE_URL; ?>webroot/images/LogoPolytech.png" alt="logo">
        <h1>Bienvenue sur PolyShop, le shop en ligne de Polytech Tours !</h1>
    </section>

    <section id="section2">
        <div class="container">
            <div class="row">
                <div class="vitrineTrois col-md-4">
                    <h4>Créé ton compte PolyShop</h4>
                    <p>Cela permet aux acheteurs de t'identifier plus facilement</p>
                </div>
                <div class="vitrineTrois col-md-4">
                    <h4>Achète des articles facilement</h4>
                    <p>Sur PolyShop, en quelques clics vous pouvez acheter n'importe quel produit mis en vente</p>
                </div>
                <div class="vitrineTrois col-md-4">
                    <h4>Poste des annonces</h4>
                    <p>Plus besoin d'aller en magasin pour revendre ses occasions, publie un annonce et vend ton bien</p>
                </div>
            </div>
        </div>
    </section>

    <footer><p>© PolyShop 2021</p></footer>
</body>

</html>