<html>

<head>

<?php

include('include/default.php'); 

?>

</head>

<body>

    <?php include('include/navigation.php'); ?>

    <section id="section6" class="container"> 

        <h1 class="boiteReception">Toutes les annonces</h1>

        <?php 
        
        if($act != false){ 
            for($i = 0; $i < count($act); $i++){ ?>
                <div onclick="location.href='<?php echo BASE_URL; ?>annonce/<?php echo $act[$i]['id']; ?>'" class="container myActivity containerGridAct">
                    <div class="item item-a" >
                        <div class="avatarPicture" style="background-image: url('<?php echo BASE_URL; ?>webroot/images/avatars/<?php echo $act[$i]['photo']; ?>');border: 1px solid black" id="avatarPicture2"></div>
                    </div>
                    <span class="actNom"><?php echo $act[$i]['nom']; ?></span>
                    <span class="actPrix"><?php echo $act[$i]['prix']; ?>€</span>
                </div>
            <?php }
        } 
        else{ ?>
            <h1>Pas d'annonce, créez en une vous même !</h1>
        <?php } ?>
        
    </section>

</body>

</html>