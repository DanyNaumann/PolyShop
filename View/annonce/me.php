<html>

<head>

<?php

include('include/default.php'); 

?>

</head>

<body>

    <?php include('include/navigation.php'); ?>

    <section id="section6" class="container"> 

        <h1 class="boiteReception">Mes annonces</h1>

        <?php 
        
        if($act != false){ 
            for($i = 0; $i < count($act); $i++){ ?>
                <div onclick="location.href='<?php echo BASE_URL; ?>annonce/<?php echo $act[$i]['id']; ?>'" class="container myActivity containerGridAct">
                    <div class="item item-a" >
                        <div class="avatarPicture" style="background-image: url('<?php echo BASE_URL; ?>webroot/images/avatars/<?php echo $act[$i]['photo']; ?>');" id="avatarPicture2"></div>
                    </div>
                    <span class="actNom"><?php echo $act[$i]['nom']; ?></span>
                    <span class="actPrix"><?php echo $act[$i]['prix']; ?>€</span>
                    <div class="editActivityLink">
                        <a href="<?php echo BASE_URL; ?>annonce/edit/<?php echo $act[$i]['id']; ?>">
                            <i class="far fa-edit"></i>Modifier
                        </a> l'annonce
                    </div>
                    <div class="supprAccountLink">
                        <a oncLick="alert('Etes vous surs de vouloir supprimer cette annonce ?');" href="<?php echo BASE_URL; ?>annonce/delete/<?php echo $act[$i]['id']; ?>">
                            <i class="far fa-trash-alt"></i> Supprimer
                        </a> l'annonce
                    </div> 
                    <div class="supprAccountLink">
                        <a href="<?php echo BASE_URL; ?>annonce/offer/<?php echo $act[$i]['id']; ?>">
                            <i class="fa fa-usd""></i> Voir
                        </a> les offres
                    </div> 
                </div>
            <?php }
        } 
        else{ ?>
            <h1>Pas d'annonce, créez en une vous même !</h1>
        <?php } ?>
        
    </section>

</body>

</html>