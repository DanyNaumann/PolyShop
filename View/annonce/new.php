<html>

<head>

<?php

include('include/default.php'); 

?>

</head>

<body>

    <?php include('include/navigation.php'); ?>

    <section id="section4" class="container">
        <div class="editForm">
            <form method="post" id="formEdit" action="<?php echo BASE_URL; ?>annonce/new/result" enctype="multipart/form-data">
                <h2 class="login-box-msg">Créer une annonce</h2>
                <div class="mailForm">
                    <div class="form-group has-feedback">
                        <label for="prix">Nom :</label>
                        <input type="text" id="nom" name="nom" class="form-control champ" placeholder="Nom" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Description :</label>
                        <textarea name="description" class="form-control" rows="5" id="description" maxlength="255" placeholder="Maximum de 255 caractères"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="prix">Prix (€) :</label>
                        <input name="prix" type="number" class="form-control" min="0" max="1000" step="1" placeholder="10">
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="avatar">Photo :</label>
                        <input name="avatar" type="file" class="form-control" id="avatar"/>
                    </div>
                    <div class="row">
                        <button type="submit" id="btnLogin" class="btn btn-primary btn-block sign-in">
                            Valider
                        </button>
                    </div>
                </div>

            </form> 
        </div>
    </section>

</body>

</html>