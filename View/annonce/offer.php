<html>

<head>

<?php

include('include/default.php'); 

?>

</head>

<body>

    <?php include('include/navigation.php'); ?>

    <section id="section6" class="container"> 

        <h1 class="boiteReception">Offres</h1>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">Utilisateur</th>
                    <th scope="col">Montant</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php for($i = 0; $i < count($offer); $i++){ ?>
                    <tr>
                        <th scope="row"><?php echo $user[$i][0]['prenom']; ?></th>
                        <th><?php echo $offer[$i]['montant']; ?>€</th>
                        <th><button onclick="alert('Vous avez accepté l offre de <?php echo $user[$i][0]['prenom']; ?> !'); window.location.href='<?php echo BASE_URL; ?>annonce/offer/accept/<?php echo $offer[0]['id_act']; ?>/<?php echo $offer[0]['id_user']; ?>/<?php echo $offer[0]['montant']; ?>'" class="validButton"><i class="fa fa-check" aria-hidden="true"></i></button></th>
                        <th><button onclick="window.location.href='<?php echo BASE_URL; ?>annonce/offer/delete/<?php echo $offer[0]['id_act']; ?>/<?php echo $offer[0]['id_user']; ?>/<?php echo $offer[0]['montant']; ?>'" class="cancelButton"><i class="fa fa-ban" aria-hidden="true"></i></button></th>
                    </tr>
                <?php } ?>
                
            </tbody>
        </table>
        
    </section>

</body>

</html>