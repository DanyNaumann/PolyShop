<html>

<head>

<?php

include('include/default.php'); 

?>

</head>

<body>

    <?php include('include/navigation.php'); ?>

    <section id="section7" class="container"> 

        <?php if($act != false){ ?>
            <h1 class="boiteReception">Annonce de <a href=""><?php echo $user[0]['prenom']; ?></a></h1>
            <div class="item item-a" >
                <div class="avatarPicture" style="background-image: url('<?php echo BASE_URL; ?>webroot/images/avatars/<?php echo $act[0]['photo']; ?>'); border: 1px solid black" id="avatarPicture3"></div>
            </div>
            <span class="actNom"><?php echo $act[0]['nom']; ?></span>
            <span class="actPrix"><?php echo $act[0]['prix']; ?>€</span>
            <span class="actDescription">"<?php echo $act[0]['description']; ?>"</span>
            <?php
                if($act[0]['id_proprio'] != $_SESSION['id']){ ?>
                    <button class="actOffer btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        <i class="fa fa-handshake-o"></i> Faire une offre
                    </button> 
            <?php } ?>
            
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Faire une offre</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="<?php echo BASE_URL; ?>annonce/<?php echo $act[0]['id']?>/offer" method="post">
                            <label for="offer">Offre (€) : </label>
                            <input name="offer" type="number" min="0" placeholder="10€" required>
                            <button type="submit" class="btn btn-primary">Valider</button>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                    </div>
                    </div>
                </div>
            </div>
        <?php } 
        else{ ?>
            <h1>Aucune activité correspondante</h1>
        <?php } ?>
        
        
    </section>

</body>

</html>