<html>

<head>

<?php include 'include/default.php'; ?>

</head>

<body>

<?php include 'include/navigation.php'; ?>

<section id="section3" class="container">
<div class="loginForm">
        <form method="post" id="formRegister" action="<?php echo BASE_URL; ?>account/edit/result" enctype="multipart/form-data">
            <h2 class="login-box-msg">Modifier le profil</h2>
            <?php if (isset($messageErreur)) {
                echo '<div id="erreur2"><p>' . $messageErreur . '</p></div>';
            } ?>
            <div class="mailForm">
                <div class="form-group has-feedback has-float-label">
                    <input type="text" name="textFirstName" class="form-control champ" value="<?php echo $profil[0]['prenom']; ?>" id="p1" required>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" name="textName" class="form-control champ" value="<?php echo $profil[0]['nom']; ?>" id="n1" required>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" id="email" name="email" class="form-control champ" value="<?php echo $profil[0]['mail']; ?>"
                            maxlength="100" required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    <span id="email-error-message"></span>
                </div>
                <div class="form-group">
                    <label class="form-label" for="avatar">Photo :</label>
                    <input name="avatar" type="file" class="form-control" id="avatar"/>
                </div>
            </div>
            <div class="row">
                <button type="submit" id="btnLogin" class="btn btn-primary btn-block sign-in">
                    Valider
                </button>
            </div>
        </form> 
    </div>
</section>

</body>

</html>