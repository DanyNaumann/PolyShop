<html>

<head>

<?php include 'include/default.php'; ?>

</head>

<body>

<?php include 'include/navigation.php'; ?>

<section id="section3" class="container">
<div class="loginForm">
        <form method="post" id="formRegister" action="<?php echo BASE_URL; ?>account/register/send">
            <h2 class="login-box-msg">Créer un compte</h2>
            <?php if (isset($messageErreur)) {
                echo '<div id="erreur2"><p>' . $messageErreur . '</p></div>';
            } ?>
            <div class="mailForm">
                <div class="form-group has-feedback has-float-label">
                    <input type="text" name="textFirstName" class="form-control champ" placeholder="Prénom" id="p1" required>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" name="textName" class="form-control champ" placeholder="Nom" id="n1" required>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="text" id="email" name="email" class="form-control champ" placeholder="E-mail"
                            maxlength="100" required>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    <span id="email-error-message"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" id="password1" name="password" class="form-control champ"
                            placeholder="Mot de passe (8 characters minimum)"
                            maxlength="255"
                            required
                    >
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    <span id="password-error-message"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" id="password2" name="password2" class="form-control champ"
                            placeholder="Entrez de nouveau votre mot de passe" maxlength="255" required>
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    <span id="password2-error-message"></span>
                </div>
            </div>
            <div class="row">
                <button type="submit" id="btnLogin" class="btn btn-primary btn-block sign-in">
                    S'enregistrer
                </button>
            </div>

            <input id="sign" name="sign" type="hidden" value="up">

            <div class="row login-msg">
                <span>Vous avez déjà un compte ? <a href="<?php echo BASE_URL; ?>account/login">Connectez-vous !</a></span>
            </div>
        </form> 
    </div>
</section>

</body>

</html>