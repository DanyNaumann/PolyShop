<html>

<head>

<?php

include('include/default.php');

?>

</head>

<body>

<?php include('include/navigation.php'); ?>

    <section id="section3" class="container">
        <div class="profil">
            <div class="divAvatar">
                <img class="avatarPicture" src="<?php echo BASE_URL; ?>webroot/images/<?php echo $profil[0]['avatar']; ?>" id="avatarPicture">
            </div>
            <h1 class="display-1"><?php echo $profil[0]['prenom'] . " " . $profil[0]['nom'];?><a href="<?php echo BASE_URL; ?>account/edit"><i class="fa fa-pencil-square-o modifyProfil" data-toggle="tooltip" data-placement="top" title="Modifier le profil" aria-hidden="true"></i></a></h1>
            <h2 class="display-2"><?php echo $profil[0]['mail']; ?></h2>
            <div class="supprAccountLink">
                <a oncLick="alert('Etes vous surs de vouloir supprimer votre compte ?');" href="<?php echo BASE_URL; ?>account/delete">
                    <i class="far fa-trash-alt"></i> Supprimer
                </a> mon compte
            </div>  
            
        </div>
    </section>


</body>

</html>