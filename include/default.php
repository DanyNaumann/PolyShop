<title>PolyShop</title>
<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/6b0be5246e.js"></script>
<link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
<link rel="stylesheet" href="<?php echo BASE_URL;?>webroot/css/style.css?v=<?php echo rand(1, 100); ?>">
<script src="<?php echo BASE_URL;?>webroot/js/script.js?v=<?php echo rand(1, 100); ?>"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-1.5.1.min.js"></script>
<script type="text/javascript" src="https://ajax.microsoft.com/ajax/jquery.ui/1.8.10/jquery-ui.js"></script>
<link rel="Stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/base/jquery-ui.css" />
<script src="https://kit.fontawesome.com/97125b6ade.js" crossorigin="anonymous"></script>