<nav class="navbar navbar-default navbar-fixed-top">

    <div class="container-fluid">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo BASE_URL; ?>homepage"><span class="logo"><img src="<?php echo BASE_URL; ?>webroot/images/LogoPolytech.png" alt="Brand logo"></span> PolyShop</a>
        </div>

        <div class="collapse navbar-collapse" id="myNavbar">

            <ul class="nav navbar-nav navbar-right">

                <?php if(isset($_SESSION['id'])){ ?>
                    <li class="navbar-item"><a class="navbarNoPadding" href="<?php echo BASE_URL; ?>annonce/all"> <i class="fas fa-stream"></i> Voir les annonces </a></li>
                    <li class="navbar-item"><a class="navbarNoPadding" href="<?php echo BASE_URL; ?>annonce/new"> <i class="fa fa-plus-circle" aria-hidden="true"></i> Publier une annonce </a></li>
                    <li class="dropdown navbar-item">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> 
                            <span><?php echo $_SESSION['firstname']; ?></span> 
                            <span class="caret"></span> 
                        </a>
                        <ul class="dropdown-menu">
                            <li class="liUp"><a href="<?php echo BASE_URL; ?>account/me"><i class="far fa-user"></i> Mon profil</a></li>
                            <hr>
                            <li><a href="<?php echo BASE_URL; ?>annonce/me"><i class="fa fa-book"></i> Mes annonces</a></li>
                            <hr>
                            <li class="liDown"><a href="<?php echo BASE_URL; ?>account/logout"><i class="fas fa-power-off"></i> Déconnexion</a></li>
                        </ul>
                    </li>
                <?php } 
                else{ ?>
                    <li class="navbar-item"><a class="navbarNoPadding" href="<?php echo BASE_URL; ?>account/register"> <i class="fa fa-user-plus" aria-hidden="true"></i> Inscription </a></li>
                    <li class="navbar-item"><a class="navbarNoPadding" href="<?php echo BASE_URL; ?>account/login"> <i class="fa fa-sign-in" aria-hidden="true"></i> Connexion </a></li>
                <?php } ?>

            </ul>
            
        </div>

    </div>

</nav>

<?php if(isset($_SESSION['flash'])) { ?>
    <div class="container conteneur">
        <div id="closeBanner" class="notifyBanner alert alert-<?php echo $_SESSION['flash'][1]; ?>">
            <?php echo $_SESSION['flash'][0]; ?>
            <button type="button" class="close" data-dismiss="alert">x</button>
        </div>
    </div>
    <?php unset($_SESSION['flash']);
} ?>