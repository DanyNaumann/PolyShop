<?php

	//On démarre la session
	session_start();

    include 'conf/conf.php';

	require_once 'Model/pdoUser.php';
	require_once 'Model/pdoAnnonce.php';
	require_once 'Model/pdoAuthentication.php';

    if (!isset($_REQUEST['uc'])) {
		$_REQUEST['uc'] = 'homepage';
	}
	$uc = $_REQUEST["uc"];

	switch ($uc) {
		case "manage_users":
			include 'Controller/C_user.php';
			break;
        case "manage_annonces":
            include 'Controller/C_annonce.php';
            break;
		case "manage_auth":
			include 'Controller/C_authentication.php';
			break;
		default:
			include 'View/accueil.php';
			break;
	}

?>